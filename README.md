This is a test suite for X.Org tests.

Building the code
=================

```
$ meson builddir
$ ninja -C builddir
```

This project relies on the xorg-gtest repo which is enabled as a submodule.

Running the tests
=================

Tests can be run by `ninja -C builddir test`, or one-by-one.

All tests except the input and video module load tests can be run inside a container.
This allows to run the tests in a regular development machine with convenient debugging
and fast turnaround. The only prerequisite is root permissions.

The most convenient way to run the tests is running `./start-unshare.sh` as root.
This creates a bare-bones environment that shares the filesystem with the rest of the system,
but is otherwise isolated from it. This allows running additional X servers without affecting the
primary X server running on the machine.

If running tests via `./start_unshare.sh` results in most tests failing with
`parse_vt_settings: Cannot open /dev/tty0 (No such file or directory)` then make sure
that the test box does not have X11 configuration that e.g. forces loading of additional
driver modules.

If the tests are run without any isolation then it is advisable to shut down any X
server on the test box.

For controlling test output (e.g. xml reporting), refer to
http://code.google.com/p/googletest/wiki/AdvancedGuide#Controlling_Test_Output

Debugging test failures
=======================

To run a subset of tests within a test case, filter on the test name. For
example,

        ./builddir/test-input-load --gtest_filter=InputDriverTest.DriverDevice/7

only runs the 7th test in the InputDriverTest/DriverDevice test case. Refer
to the googletest wiki for more information.
http://code.google.com/p/googletest/wiki/AdvancedGuide#Running_a_Subset_of_the_Tests

xorg-gtest supports the environment variable XORG_GTEST_XSERVER_SIGSTOP. If
set, the test will SIGSTOP itself once a server has been started. This
allows to investigate logs or attach gdb to the server process.

For further environment variables please refer to the xorg-gtest README
http://gitlab.freedesktop.org/xorg/test/xorg-gtest/

Writing tests
=============

Please refer to the HACKING document
