/*******************************************************************************
 *
 * X testing environment - Google Test environment feat. dummy x server
 *
 * Copyright (C) 2020 Povilas Kanapickas <povilas@radix.lt>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ******************************************************************************/

#include "inputtest-driver-connection.h"

#include <X11/extensions/XInput2.h>
#include <xf86-input-inputtest-protocol.h>

#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <cstring>
#include <string>
#include <stdexcept>
#include <thread>

namespace xorg {
namespace testing {
namespace inputtest {

namespace {

constexpr std::int32_t XORG_KEY_CODE_OFFSET = 8;

template<class T>
void SendEvent(int fd, const T& ev)
{
    if (write(fd, &ev, ev.header.length) != ev.header.length)
        throw std::runtime_error("Failed to send event");
}

template<class T>
T CreateEvent(xf86ITEventType type)
{
    T event;
    std::memset(&event, 0, sizeof(event));
    event.header.length = sizeof(event);
    event.header.type = type;
    return event;
}

void SendButtonEvent(int fd, bool is_press, bool is_absolute, std::int32_t button,
                     const Valuators& valuators)
{
    xf86ITEventButton ev = CreateEvent<xf86ITEventButton>(XF86IT_EVENT_BUTTON);
    ev.is_absolute = is_absolute;
    ev.button = button;
    ev.is_press = is_press;
    valuators.RetrieveValuatorData(&ev.valuators);
    SendEvent(fd, ev);
}

void SendTouchEvent(int fd, std::uint16_t type, std::uint32_t touchid, const Valuators& valuators)
{
    xf86ITEventTouch ev = CreateEvent<xf86ITEventTouch>(XF86IT_EVENT_TOUCH);
    ev.touch_type = type;
    ev.touchid = touchid;
    valuators.RetrieveValuatorData(&ev.valuators);
    SendEvent(fd, ev);
}

void SendGesturePinchEvent(int fd, std::uint16_t type, std::uint32_t flags,
                           std::uint16_t num_touches, double delta_x, double delta_y,
                           double delta_unaccel_x, double delta_unaccel_y,
                           double scale, double delta_angle)
{
    xf86ITEventGesturePinch ev = CreateEvent<xf86ITEventGesturePinch>(XF86IT_EVENT_GESTURE_PINCH);
    ev.gesture_type = type;
    ev.num_touches = num_touches;
    ev.flags = flags;
    ev.delta_x = delta_x;
    ev.delta_y = delta_y;
    ev.delta_unaccel_x = delta_unaccel_x;
    ev.delta_unaccel_y = delta_unaccel_y;
    ev.scale = scale;
    ev.delta_angle = delta_angle;
    SendEvent(fd, ev);
}

void SendGestureSwipeEvent(int fd, std::uint16_t type, std::uint32_t flags,
                           std::uint16_t num_touches, double delta_x, double delta_y,
                           double delta_unaccel_x, double delta_unaccel_y)
{
    xf86ITEventGestureSwipe ev = CreateEvent<xf86ITEventGestureSwipe>(XF86IT_EVENT_GESTURE_SWIPE);
    ev.gesture_type = type;
    ev.num_touches = num_touches;
    ev.flags = flags;
    ev.delta_x = delta_x;
    ev.delta_y = delta_y;
    ev.delta_unaccel_x = delta_unaccel_x;
    ev.delta_unaccel_y = delta_unaccel_y;
    SendEvent(fd, ev);
}

template<class T>
T ReceiveResponse(int fd, int type)
{
    T response;
    std::memset(&response, 0, sizeof(response));
    if (read(fd, &response, sizeof(response)) != sizeof(response))
        throw std::runtime_error("Could not read event " + std::to_string(type));
    if (response.header.type != type)
        throw std::runtime_error("Invalid response type: got " + std::to_string(response.header.type) +
                               " expected " + std::to_string(type));
    if (response.header.length != sizeof(response))
        throw std::runtime_error("Invalid response length");
    return response;
}

} // namespace

struct DriverConnection::Private {
    std::string socket_path;
    std::uint32_t protocol_version_major = 0;
    std::uint32_t protocol_version_minor = 0;
    int socket_fd = -1;
};

DriverConnection::DriverConnection(const std::string& socket_path) :
    d_{std::unique_ptr<Private>(new Private)}
{
    d_->socket_path = socket_path;
}

DriverConnection::~DriverConnection()
{
    Close();
}

const std::string& DriverConnection::SocketPath() const
{
    return d_->socket_path;
}

bool DriverConnection::TryConnectToDriver()
{
    if (d_->socket_fd != -1) {
        return true;
    }

    sockaddr_un address = {};

    if (d_->socket_path.size() > sizeof(address.sun_path))
        throw std::runtime_error("Too long path to connect to driver");

    address.sun_family = AF_UNIX;
    std::strncpy(address.sun_path, d_->socket_path.c_str(), sizeof(address.sun_path));

    if (access(d_->socket_path.c_str(), F_OK) == -1) {
        return false;
    }
    int socket_fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if (socket_fd < 0)
        throw std::runtime_error("Can't create socket");

    if (connect(socket_fd, reinterpret_cast<sockaddr*>(&address), sizeof(address)) != 0) {
        close(socket_fd);
        return false;
    }

    d_->socket_fd = socket_fd;
    return true;
}

void DriverConnection::WaitForDriver()
{
    unsigned timeout_ms = 10000;
    unsigned sleep_ms = 20;

    if (d_->socket_fd != -1)
        return;

    for (unsigned i = 0; i < timeout_ms; i += sleep_ms) {
        if (!TryConnectToDriver()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(sleep_ms));
            continue;
        }

        xf86ITEventClientVersion ev = CreateEvent<xf86ITEventClientVersion>(XF86IT_EVENT_CLIENT_VERSION);
        ev.major = XF86IT_PROTOCOL_VERSION_MAJOR;
        ev.minor = XF86IT_PROTOCOL_VERSION_MINOR;
        SendEvent(d_->socket_fd, ev);

        xf86ITResponseServerVersion response =
            ReceiveResponse<xf86ITResponseServerVersion>(d_->socket_fd, XF86IT_RESPONSE_SERVER_VERSION);

        d_->protocol_version_major = response.major;
        d_->protocol_version_minor = response.minor;
        return;
    }

    throw std::runtime_error("Timed out waiting for device files to be opened");
}

void DriverConnection::Close()
{
    if (d_->socket_fd != -1) {
        close(d_->socket_fd);
        d_->socket_fd = -1;
    }
}

void DriverConnection::Sync()
{
    xf86ITEventWaitForSync ev = CreateEvent<xf86ITEventWaitForSync>(XF86IT_EVENT_WAIT_FOR_SYNC);
    SendEvent(d_->socket_fd, ev);
    ReceiveResponse<xf86ITResponseSyncFinished>(d_->socket_fd, XF86IT_RESPONSE_SYNC_FINISHED);
}

void DriverConnection::EnsureConnectedToDriver()
{
    if (d_->socket_fd == -1)
        throw std::runtime_error("DriverConnection is not open");
}

void DriverConnection::EnsureVersionMinor(unsigned major, unsigned minor)
{
    if (d_->protocol_version_major != major || d_->protocol_version_minor < minor)
        throw std::runtime_error("Server does not support event type");
}

void DriverConnection::RelMotion(const Valuators& valuators)
{
    EnsureConnectedToDriver();
    xf86ITEventMotion ev = CreateEvent<xf86ITEventMotion>(XF86IT_EVENT_MOTION);
    ev.is_absolute = false;
    valuators.RetrieveValuatorData(&ev.valuators);
    SendEvent(d_->socket_fd, ev);
    Sync();
}

void DriverConnection::AbsMotion(const Valuators& valuators)
{
    EnsureConnectedToDriver();
    xf86ITEventMotion ev = CreateEvent<xf86ITEventMotion>(XF86IT_EVENT_MOTION);
    ev.is_absolute = true;
    valuators.RetrieveValuatorData(&ev.valuators);
    SendEvent(d_->socket_fd, ev);
    Sync();
}

void DriverConnection::ProximityIn(const Valuators& valuators)
{
    EnsureConnectedToDriver();
    xf86ITEventProximity ev = CreateEvent<xf86ITEventProximity>(XF86IT_EVENT_PROXIMITY);
    ev.is_prox_in = true;
    valuators.RetrieveValuatorData(&ev.valuators);
    SendEvent(d_->socket_fd, ev);
    Sync();
}

void DriverConnection::ProximityOut(const Valuators& valuators)
{
    EnsureConnectedToDriver();
    xf86ITEventProximity ev = CreateEvent<xf86ITEventProximity>(XF86IT_EVENT_PROXIMITY);
    ev.is_prox_in = false;
    valuators.RetrieveValuatorData(&ev.valuators);
    SendEvent(d_->socket_fd, ev);
    Sync();
}

void DriverConnection::ButtonDownAbs(std::int32_t button, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendButtonEvent(d_->socket_fd, true, true, button, valuators);
    Sync();
}

void DriverConnection::ButtonDownRel(std::int32_t button, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendButtonEvent(d_->socket_fd, true, false, button, valuators);
    Sync();
}

void DriverConnection::ButtonUpAbs(std::int32_t button, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendButtonEvent(d_->socket_fd, false, true, button, valuators);
    Sync();
}

void DriverConnection::ButtonUpRel(std::int32_t button, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendButtonEvent(d_->socket_fd, false, false, button, valuators);
    Sync();
}

void DriverConnection::KeyDown(std::int32_t key_code)
{
    EnsureConnectedToDriver();
    xf86ITEventKey ev = CreateEvent<xf86ITEventKey>(XF86IT_EVENT_KEY);
    ev.is_press = true;
    ev.key_code = key_code + XORG_KEY_CODE_OFFSET;
    SendEvent(d_->socket_fd, ev);
    Sync();
}

void DriverConnection::KeyUp(std::int32_t key_code)
{
    EnsureConnectedToDriver();
    xf86ITEventKey ev = CreateEvent<xf86ITEventKey>(XF86IT_EVENT_KEY);
    ev.is_press = false;
    ev.key_code = key_code + XORG_KEY_CODE_OFFSET;
    SendEvent(d_->socket_fd, ev);
    Sync();
}

void DriverConnection::TouchBegin(std::uint32_t id, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendTouchEvent(d_->socket_fd, XI_TouchBegin, id, valuators);
    Sync();
}

void DriverConnection::TouchUpdate(std::uint32_t id, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendTouchEvent(d_->socket_fd, XI_TouchUpdate, id, valuators);
    Sync();
}

void DriverConnection::TouchEnd(std::uint32_t id, const Valuators& valuators)
{
    EnsureConnectedToDriver();
    SendTouchEvent(d_->socket_fd, XI_TouchEnd, id, valuators);
    Sync();
}

void DriverConnection::GestureSwipeBegin(std::uint16_t num_touches,
                                         double delta_x, double delta_y,
                                         double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGestureSwipeEvent(d_->socket_fd, XI_GestureSwipeBegin, 0,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y);
    Sync();
}

void DriverConnection::GestureSwipeUpdate(std::uint16_t num_touches,
                                          double delta_x, double delta_y,
                                          double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGestureSwipeEvent(d_->socket_fd, XI_GestureSwipeUpdate, 0,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y);
    Sync();
}

void DriverConnection::GestureSwipeEnd(std::uint16_t num_touches,
                                       double delta_x, double delta_y,
                                       double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGestureSwipeEvent(d_->socket_fd, XI_GestureSwipeEnd, 0,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y);
    Sync();
}

void DriverConnection::GestureSwipeCancel(std::uint16_t num_touches,
                                          double delta_x, double delta_y,
                                          double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGestureSwipeEvent(d_->socket_fd, XI_GestureSwipeEnd, XIGestureSwipeEventCancelled,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y);
    Sync();
}

void DriverConnection::GesturePinchBegin(std::uint16_t num_touches,
                                         double delta_x, double delta_y,
                                         double delta_unaccel_x, double delta_unaccel_y,
                                         double scale, double delta_angle)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGesturePinchEvent(d_->socket_fd, XI_GesturePinchBegin, 0,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y,
                          scale, delta_angle);
    Sync();
}

void DriverConnection::GesturePinchUpdate(std::uint16_t num_touches,
                                          double delta_x, double delta_y,
                                          double delta_unaccel_x, double delta_unaccel_y,
                                          double scale, double delta_angle)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGesturePinchEvent(d_->socket_fd, XI_GesturePinchUpdate, 0,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y,
                          scale, delta_angle);
    Sync();
}

void DriverConnection::GesturePinchEnd(std::uint16_t num_touches,
                                       double delta_x, double delta_y,
                                       double delta_unaccel_x, double delta_unaccel_y,
                                       double scale, double delta_angle)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGesturePinchEvent(d_->socket_fd, XI_GesturePinchEnd, 0,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y,
                          scale, delta_angle);
    Sync();
}

void DriverConnection::GesturePinchCancel(std::uint16_t num_touches,
                                          double delta_x, double delta_y,
                                          double delta_unaccel_x, double delta_unaccel_y,
                                          double scale, double delta_angle)
{
    EnsureConnectedToDriver();
    EnsureVersionMinor(1, 1);
    SendGesturePinchEvent(d_->socket_fd, XI_GesturePinchEnd, XIGesturePinchEventCancelled,
                          num_touches, delta_x, delta_y, delta_unaccel_x, delta_unaccel_y,
                          scale, delta_angle);
    Sync();
}

} // namespace xorg
} // namespace testing
} // namespace inputtest
