/*******************************************************************************
 *
 * X testing environment - Google Test environment feat. dummy x server
 *
 * Copyright (C) 2020 Povilas Kanapickas <povilas@radix.lt>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ******************************************************************************/

#include "xorg/gtest/inputtest/xorg-gtest-device.h"
#include "inputtest-driver-connection.h"

#include <X11/extensions/XInput2.h>

#include <fcntl.h>
#include <unistd.h>

#include <cstring>
#include <stdexcept>
#include <thread>

namespace xorg {
namespace testing {
namespace inputtest {

namespace {

std::string DeviceTypeToOptionString(DeviceType type)
{
    switch (type) {
        case DeviceType::KEYBOARD: return "Keyboard";
        case DeviceType::POINTER: return "Pointer";
        case DeviceType::POINTER_GESTURE: return "PointerGesture";
        case DeviceType::POINTER_ABSOLUTE: return "PointerAbsolute";
        case DeviceType::POINTER_ABSOLUTE_PROXIMITY: return "PointerAbsoluteProximity";
        case DeviceType::TOUCH: return "Touch";
    }
    return "";
}

void EnsureDeviceTypeForEvent(DeviceType type, std::initializer_list<DeviceType> allowed_types)
{
    for (auto allowed_type : allowed_types) {
    if (type == allowed_type)
        return;
    }
    throw std::runtime_error("Unsupported event for the device type");
}

} // namespace

struct Device::Private {
    Private(const std::string& socket_path) :
        connection(socket_path) {}

    DriverConnection connection;
    DeviceType device_type;
};

Device::Device(const std::string& socket_path, DeviceType type) :
    d_{std::unique_ptr<Private>(new Private(socket_path))}
{
    d_->device_type = type;
}

Device::~Device()
{
}

std::string Device::GetOptions() const
{
    // add a new line before to protect against accidentally missed newline in the rest of
    // configuration
    return "\n"
        "Option \"DeviceType\" \"" + DeviceTypeToOptionString(d_->device_type) + "\"\n"
        "Option \"SocketPath\" \"" + d_->connection.SocketPath() + "\"\n";
}

void Device::WaitForDriver()
{
    d_->connection.WaitForDriver();
}

void Device::RelMotion(double x, double y)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER, DeviceType::POINTER_GESTURE});
    Valuators valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.RelMotion(valuators);
}

void Device::RelMotion(double x, double y, const Valuators& extra_valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER, DeviceType::POINTER_GESTURE});
    Valuators valuators = extra_valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.RelMotion(valuators);
}

void Device::AbsMotion(double x, double y)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_ABSOLUTE});
    Valuators valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.AbsMotion(valuators);
}

void Device::AbsMotion(double x, double y, const Valuators& extra_valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_ABSOLUTE});
    Valuators valuators = extra_valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.AbsMotion(valuators);
}

void Device::ProximityIn(const Valuators& valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_ABSOLUTE_PROXIMITY});
    d_->connection.ProximityIn(valuators);
}

void Device::ProximityOut(const Valuators& valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_ABSOLUTE_PROXIMITY});
    d_->connection.ProximityOut(valuators);
}

void Device::ButtonDown(std::int32_t button)
{
    EnsureDeviceTypeForEvent(d_->device_type, {
        DeviceType::POINTER,
        DeviceType::POINTER_GESTURE,
        DeviceType::POINTER_ABSOLUTE,
        DeviceType::POINTER_ABSOLUTE_PROXIMITY,
    });
    if (button == 0 || button >= 256)
        throw std::runtime_error("Invalid button number");

    if (d_->device_type == DeviceType::POINTER || d_->device_type == DeviceType::POINTER_GESTURE) {
        d_->connection.ButtonDownRel(button, Valuators());
    } else {
        d_->connection.ButtonDownAbs(button, Valuators());
    }
}

void Device::ButtonUp(std::int32_t button)
{
    EnsureDeviceTypeForEvent(d_->device_type, {
        DeviceType::POINTER,
        DeviceType::POINTER_GESTURE,
        DeviceType::POINTER_ABSOLUTE,
        DeviceType::POINTER_ABSOLUTE_PROXIMITY,
    });
    if (button == 0 || button >= 256)
        throw std::runtime_error("Invalid button number");

    if (d_->device_type == DeviceType::POINTER || d_->device_type == DeviceType::POINTER_GESTURE) {
        d_->connection.ButtonUpRel(button, Valuators());
    } else {
        d_->connection.ButtonUpAbs(button, Valuators());
    }
}

void Device::KeyDown(std::int32_t key_code)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::KEYBOARD});
    d_->connection.KeyDown(key_code);
}

void Device::KeyUp(std::int32_t key_code)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::KEYBOARD});
    d_->connection.KeyUp(key_code);
}

void Device::TouchBegin(double x, double y, std::uint32_t id)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::TOUCH});
    Valuators valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.TouchBegin(id, valuators);
}

void Device::TouchBegin(double x, double y, std::uint32_t id, const Valuators& extra_valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::TOUCH});
    Valuators valuators = extra_valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.TouchBegin(id, valuators);
}

void Device::TouchUpdate(double x, double y, std::uint32_t id)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::TOUCH});
    Valuators valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.TouchUpdate(id, valuators);
}

void Device::TouchUpdate(double x, double y, std::uint32_t id, const Valuators& extra_valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::TOUCH});
    Valuators valuators = extra_valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.TouchUpdate(id, valuators);
}

void Device::TouchEnd(double x, double y, std::uint32_t id)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::TOUCH});
    Valuators valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.TouchEnd(id, valuators);
}

void Device::TouchEnd(double x, double y, std::uint32_t id, const Valuators& extra_valuators)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::TOUCH});
    Valuators valuators = extra_valuators;
    valuators.Set(0, x);
    valuators.Set(1, y);
    d_->connection.TouchEnd(id, valuators);
}

void Device::GestureSwipeBegin(std::uint16_t num_touches, double delta_x, double delta_y,
                               double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GestureSwipeBegin(num_touches, delta_x, delta_y,
                                     delta_unaccel_x, delta_unaccel_y);
}

void Device::GestureSwipeUpdate(std::uint16_t num_touches, double delta_x, double delta_y,
                                double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GestureSwipeUpdate(num_touches, delta_x, delta_y,
                                      delta_unaccel_x, delta_unaccel_y);
}

void Device::GestureSwipeEnd(std::uint16_t num_touches, double delta_x, double delta_y,
                             double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GestureSwipeEnd(num_touches, delta_x, delta_y,
                                   delta_unaccel_x, delta_unaccel_y);
}

void Device::GestureSwipeCancel(std::uint16_t num_touches, double delta_x, double delta_y,
                                double delta_unaccel_x, double delta_unaccel_y)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GestureSwipeCancel(num_touches, delta_x, delta_y,
                                      delta_unaccel_x, delta_unaccel_y);
}

void Device::GesturePinchBegin(std::uint16_t num_touches, double delta_x, double delta_y,
                               double delta_unaccel_x, double delta_unaccel_y,
                               double scale, double delta_angle)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GesturePinchBegin(num_touches, delta_x, delta_y,
                                     delta_unaccel_x, delta_unaccel_y, scale, delta_angle);
}

void Device::GesturePinchUpdate(std::uint16_t num_touches, double delta_x, double delta_y,
                                double delta_unaccel_x, double delta_unaccel_y,
                                double scale, double delta_angle)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GesturePinchUpdate(num_touches, delta_x, delta_y,
                                      delta_unaccel_x, delta_unaccel_y, scale, delta_angle);
}

void Device::GesturePinchEnd(std::uint16_t num_touches, double delta_x, double delta_y,
                             double delta_unaccel_x, double delta_unaccel_y,
                             double scale, double delta_angle)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GesturePinchEnd(num_touches, delta_x, delta_y,
                                   delta_unaccel_x, delta_unaccel_y, scale, delta_angle);
}

void Device::GesturePinchCancel(std::uint16_t num_touches, double delta_x, double delta_y,
                                double delta_unaccel_x, double delta_unaccel_y,
                                double scale, double delta_angle)
{
    EnsureDeviceTypeForEvent(d_->device_type, {DeviceType::POINTER_GESTURE});
    d_->connection.GesturePinchCancel(num_touches, delta_x, delta_y,
                                      delta_unaccel_x, delta_unaccel_y, scale, delta_angle);
}

} // namespace xorg
} // namespace testing
} // namespace inputtest
