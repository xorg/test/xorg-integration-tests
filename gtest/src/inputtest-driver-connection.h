/*******************************************************************************
 *
 * X testing environment - Google Test environment feat. dummy x server
 *
 * Copyright (C) 2020 Povilas Kanapickas <povilas@radix.lt>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ******************************************************************************/

#ifndef XORG_INPUTTEST_DRIVER_CONNECTION_H_
#define XORG_INPUTTEST_DRIVER_CONNECTION_H_

#include <memory>
#include <string>
#include <cstdint>
#include "xorg/gtest/inputtest/xorg-gtest-valuators.h"

namespace xorg {
namespace testing {
namespace inputtest {

/**
 * A connection to replay events through the xf86-input-inputtest input driver
 */

class DriverConnection {
public:
    /**
     * Create a new driver connection.
     *
     * @param [in] socket_path Path to the domain socket that will be used to communicate with the
     *             driver
    */
    explicit DriverConnection(const std::string& socket_path);
    ~DriverConnection();

    /**
     * Returns the events input path
    */
    const std::string& SocketPath() const;

    /**
     * Waits for the connection to the driver being established.
     * Throws an exception if this can not be achieved within 10 seconds.
    */
    void WaitForDriver();

    /**
     * Closes the connection if it is open
    */
    void Close();

    /**
     * Synchronizes with the X server. Sends a synchronization event and waits for response.
     * This is done after all input events and is thus unnecessary in regular circumstances.
    */
    void Sync();

    /**
     * All of the functions below result in one input event being sent to the X server
     * via the FIFO files. If the files are not open yet, an exception is thrown.
    */
    void RelMotion(const Valuators& valuators);

    void AbsMotion(const Valuators& valuators);

    void ProximityIn(const Valuators& valuators);
    void ProximityOut(const Valuators& valuators);

    void ButtonDownAbs(std::int32_t button, const Valuators& valuators);
    void ButtonDownRel(std::int32_t button, const Valuators& valuators);

    void ButtonUpAbs(std::int32_t button, const Valuators& valuators);
    void ButtonUpRel(std::int32_t button, const Valuators& valuators);

    void KeyDown(std::int32_t key_code);
    void KeyUp(std::int32_t key_code);

    void TouchBegin(std::uint32_t id, const Valuators& valuators);
    void TouchUpdate(std::uint32_t id, const Valuators& valuators);
    void TouchEnd(std::uint32_t id, const Valuators& valuators);

    void GestureSwipeBegin(std::uint16_t num_touches, double delta_x, double delta_y,
                           double delta_unaccel_x, double delta_unaccel_y);
    void GestureSwipeUpdate(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y);
    void GestureSwipeEnd(std::uint16_t num_touches, double delta_x, double delta_y,
                         double delta_unaccel_x, double delta_unaccel_y);
    void GestureSwipeCancel(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y);

    void GesturePinchBegin(std::uint16_t num_touches, double delta_x, double delta_y,
                           double delta_unaccel_x, double delta_unaccel_y,
                           double scale, double delta_angle);
    void GesturePinchUpdate(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y,
                            double scale, double delta_angle);
    void GesturePinchEnd(std::uint16_t num_touches, double delta_x, double delta_y,
                         double delta_unaccel_x, double delta_unaccel_y,
                         double scale, double delta_angle);
    void GesturePinchCancel(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y,
                            double scale, double delta_angle);
private:
    struct Private;
    std::unique_ptr<Private> d_;

    bool TryConnectToDriver();
    void EnsureConnectedToDriver();
    void EnsureVersionMinor(unsigned major, unsigned minor);
};

} // namespace inputtest
} // namespace testing
} // namespace xorg

#endif // XORG_INPUTTEST_DRIVER_CONNECTION_H_
