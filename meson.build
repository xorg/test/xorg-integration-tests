project('xorg-integration-tests', 'cpp',
	version: '0.0.1',
	license: 'MIT/Expat',
	default_options: ['warning_level=2', 'cpp_std=c++11'])

pkgconfig = import('pkgconfig')
dep_x11 = dependency('x11')

dep_inputproto24 = dependency('inputproto', version: '>= 2.3.99.1', required: false)
if dep_inputproto24.found()
    add_project_arguments('-DHAVE_XI24=1', language: 'cpp')
endif

dep_xi = dependency('xi')
dep_xext = dependency('xext')
dep_xrandr = dependency('xrandr')
dep_xtst = dependency('xtst')
dep_xfixes = dependency('xfixes')
dep_xxf86dga = dependency('xxf86dga')
dep_xscrnsaver = dependency('xscrnsaver', required: false)
if dep_xscrnsaver.found()
	add_project_arguments('-DHAVE_XSCREENSAVER=1', language: 'cpp')
else
	dep_xscrnsaver = declare_dependency()
endif

dep_pixman = dependency('pixman-1')
dep_libevdev = dependency('libevdev')
dep_evemu = dependency('evemu')
dep_wacom = dependency('xorg-wacom')
dep_xserver = dependency('xorg-server')

dummy_conf = join_paths(meson.current_source_dir(), 'gtest', 'data', 'xorg', 'gtest', 'dummy.conf')

add_project_arguments('-DRECORDINGS_DIR="@0@/recordings/"'.format(meson.current_source_dir()), language: 'cpp')
# Disable two warnings which account for 95% of the compiler complaints.
# Really, these should be fixed but that requires that this project is maintained
add_project_arguments('-Wno-non-virtual-dtor', language: 'cpp')
add_project_arguments('-Wno-unused-parameter', language: 'cpp')
add_project_arguments('-Wno-missing-field-initializers', language: 'cpp')

#
# build gtest itself as static library
#
gtest_sources = files(
    'gtest/gtest/include/gtest/gtest.h',
    'gtest/gtest/include/gtest/gtest-spi.h',
    'gtest/gtest/src/gtest-all.cc',
)
gtest_includes = [
    include_directories('gtest/gtest/include'),
    include_directories('gtest/gtest/'),
]
gtest_deps = [
    dependency('threads'),
]
libgtest = static_library('gtest',
                          gtest_sources,
                          dependencies: gtest_deps,
                          include_directories: gtest_includes)
dep_gtest = declare_dependency(link_with: libgtest)

xorg_gtest_sources = files(
    'gtest/include/xorg/gtest/xorg-gtest-environment.h',
    'gtest/include/xorg/gtest/xorg-gtest-process.h',
    'gtest/include/xorg/gtest/xorg-gtest-test.h',
    'gtest/include/xorg/gtest/xorg-gtest-xserver.h',
    'gtest/include/xorg/gtest/inputtest/xorg-gtest-device.h',
    'gtest/include/xorg/gtest/inputtest/xorg-gtest-valuators.h',
    'gtest/include/xorg/gtest/xorg-gtest.h',

    'gtest/src/defines.h',
    'gtest/src/xorg-gtest_main.cpp',
    'gtest/src/environment.cpp',
    'gtest/src/process.cpp',
    'gtest/src/test.cpp',
    'gtest/src/inputtest-device.cpp',
    'gtest/src/inputtest-driver-connection.cpp',
    'gtest/src/inputtest-driver-connection.h',
    'gtest/src/valuators.cpp',
    'gtest/src/xserver.cpp',
)

xorg_gtest_incs = [
    gtest_includes,
    include_directories('gtest/include'),
]
xorg_gtest_dependencies = [
    dep_gtest,
    dep_evemu,
    dep_x11,
    dep_xi,
    dep_xserver,
]

lib_xorg_gtest = static_library('xorg-gtest',
                                xorg_gtest_sources,
                                include_directories: xorg_gtest_incs,
                                dependencies: xorg_gtest_dependencies,
                                cpp_args: [
                                    '-DDUMMY_CONF_PATH="@0@"'.format(dummy_conf),
                                    '-DLOGFILE_DIR="@0@"'.format(get_option('logpath')),
                                ],
                                )

dep_xorg_gtest = declare_dependency(link_with: lib_xorg_gtest,
                                    dependencies: [dep_xserver],
                                    include_directories: [gtest_includes, xorg_gtest_incs])


#
# gtest tests
#

executable('process-test-helper',
           'gtest/test/process-test-helper.cpp',
            install: false)
test('test-process',
     executable('test-process',
               'gtest/test/process-test.cpp',
               dependencies: dep_xorg_gtest,
               cpp_args: [
                   '-DTEST_BUILD_DIR="@0@/"'.format(meson.current_build_dir()),
               ],
               install: false,
     ),
     is_parallel: false,
)
executable('xserver-test-helper',
           'gtest/test/xserver-test-helper.cpp',
           install: false)
test('test-gtest-xserver',
    executable('test-gtest-xserver',
               'gtest/test/xserver-test.cpp',
               dependencies: dep_xorg_gtest,
               cpp_args: [
                   '-DTEST_BUILD_DIR="@0@/"'.format(meson.current_build_dir()),
                   '-DDUMMY_CONF_PATH="@0@"'.format(dummy_conf),
                   '-DLOGFILE_DIR="@0@"'.format(get_option('logpath')),
               ],
               install: false,
    ),
    is_parallel: false,
)

#
# common bits
#
common_sources = files(
		 'tests/common/xit-event.h',
		 'tests/common/xit-property.h',
		 'tests/common/xit-property.cpp',
		 'tests/common/xit-server-test.cpp',
		 'tests/common/xit-server-test.h',
		 'tests/common/xit-server.cpp',
		 'tests/common/xit-server.h',
		 'tests/common/xit-server-input-test.cpp',
		 'tests/common/xit-server-input-test.h',
		 'tests/common/xorg-conf.cpp',
		 'tests/common/xorg-conf.h',
		 'tests/common/helpers.cpp',
		 'tests/common/helpers.h',
		 'tests/common/device-inputtest-interface.cpp',
		 'tests/common/device-inputtest-interface.h',
		 'tests/common/video-driver-test.h',
)

xit_includes = [
    xorg_gtest_incs,
	include_directories('tests/common'),
]
xit_deps = [
	dep_x11,
	dep_xi,
	dep_xext,
	dep_xrandr,
	dep_xtst,
	dep_xfixes,
	dep_xscrnsaver,
	dep_xxf86dga,
	dep_pixman,
	dep_libevdev,
	dep_evemu,
	dep_wacom,
	dep_xserver,
	dep_xorg_gtest,
]
lib_xit = static_library('xit',
			 common_sources,
			 include_directories: [
				 xit_includes,
			 ],
			 cpp_args: [
				 '-DLOG_BASE_PATH="@0@"'.format(get_option('logpath')),
			 ],
			 dependencies: xit_deps

)
dep_xit = declare_dependency(link_with: lib_xit,
			     dependencies: [dep_xorg_gtest],
			     include_directories: xit_includes)


dep_tests = [
    dep_xi, dep_xit,
]

#
# input tests
#
test('test-input-module-load',
	executable('test-input-module-load',
		   'tests/input/input-module-load.cpp',
		   dependencies: dep_tests,
	),
	is_parallel: false,
	suite: 'input',
)

#
# library tests
#
test('test-libX11',
	executable('test-libX11',
		   'tests/lib/libX11.cpp',
		   dependencies: dep_tests,
	),
	is_parallel: false,
	suite: 'libs',
)
test('test-libXi',
	executable('test-libXi',
		   'tests/lib/libXi.cpp',
		   dependencies: dep_tests,
	),
	is_parallel: false,
	suite: 'libs',
)

#
# xserver tests
#
test('test-xserver',
	executable('test-xserver',
		   'tests/server/dga.cpp',
		   'tests/server/grab.cpp',
		   'tests/server/gestures.cpp',
		   'tests/server/grab-gestures.cpp',
		   'tests/server/xigrabbutton.cpp',
		   'tests/server/touch.cpp',
		   'tests/server/xtest.cpp',
		   'tests/server/multihead.cpp',
		   'tests/server/misc.cpp',
		   'tests/server/input.cpp',
		   'tests/server/barriers-common.h',
		   'tests/server/barriers-validity.cpp',
		   'tests/server/barriers-directions.cpp',
		   'tests/server/barriers-devices.cpp',
		   'tests/server/barriers-notify.cpp',
		   dependencies: [dep_tests, dep_pixman],
	),
	is_parallel: false,
	suite: 'server',
)
test('test-xephyr',
	executable('test-xephyr',
		   'tests/server/xephyr.cpp',
		   dependencies: dep_tests,
	),
	is_parallel: false,
	suite: 'server',
)

#
# video tests
#
test('test-video-module-load',
	executable('test-video-module-load',
		   'tests/video/video-module-load.cpp',
		   dependencies: [dep_tests],
	),
	is_parallel: false,
	suite: 'video',
)
