#!/bin/bash

if (( $EUID != 0 )); then
    echo "Please run this script as root"
    exit
fi

unshare --ipc --mount --mount-proc --net --pid --fork \
    /bin/bash -c "IN_CONTAINER=1 ./start-unshare-impl.sh"
