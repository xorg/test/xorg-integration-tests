/*
 * Copyright © 2012-2013 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include <math.h>
#include <pixman.h>
#include <fcntl.h>

#include <xorg/gtest/xorg-gtest.h>
#include <xorg/gtest/inputtest/xorg-gtest-device.h>

#include <X11/Xatom.h>
#include <X11/extensions/XInput2.h>
#include <X11/extensions/XInput.h>

#include <linux/input-event-codes.h>

#include "xit-server-input-test.h"
#include "xit-event.h"
#include "xit-property.h"
#include "device-inputtest-interface.h"
#include "helpers.h"

using namespace xorg::testing;

class PointerMotionTest : public XITServerInputTest,
                          public DeviceInputTestInterface {
public:
    /**
     * Initializes a standard mouse device.
     */
    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER);
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        XITServerInputTest::SetUp();
    }

    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"CorePointer\" \"on\"\n" +
                               Dev(0).GetOptions());
        // add default keyboard device
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-kbd--",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(1).GetOptions());
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};

class PointerMotionAbsoluteTest : public XITServerInputTest,
                                  public DeviceInputTestInterface {
public:
    /**
     * Initializes a standard mouse device.
     */
    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER_ABSOLUTE);
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        XITServerInputTest::SetUp();
    }

    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"CorePointer\" \"on\"\n" +
                               Dev(0).GetOptions());
        // add default keyboard device
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-kbd--",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(1).GetOptions());
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};

class PointerMotionPressureTest : public XITServerInputTest,
                                  public DeviceInputTestInterface {
    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER_ABSOLUTE);
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        XITServerInputTest::SetUp();
    }

    void RequireXI2(int major, int minor, int *major_return, int *minor_return) override {
        // don't require XI2
    }

    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"PointerHasPressure\" \"on\"\n"+
                               Dev(0).GetOptions());
        // add default keyboard device
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-kbd--",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(1).GetOptions());
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};

TEST_F(PointerMotionPressureTest, MissingValuator1InEvent)
{
    XORG_TESTCASE("Create pointer device\n"
                  "Register for XI 1.x motion events\n"
                  "Move pointer position x, leave y umodified\n"
                  "Ensure that valuator 1 is set correctly in XI event\n");

    ::Display *dpy = Display();

    int deviceid;
    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);

    const int pressure_axis = 4;

    xorg::testing::inputtest::Valuators valuators;
    valuators.Set(pressure_axis, 56);
    Dev(0).AbsMotion(1000, 1000, valuators);

    XDevice *xdev = XOpenDevice(dpy, deviceid);
    ASSERT_TRUE(xdev);

    int motion_type;
    XEventClass evclass;
    DeviceMotionNotify(xdev, motion_type, evclass);
    XSelectExtensionEvent(dpy, DefaultRootWindow(dpy), &evclass, 1);

    valuators.Set(pressure_axis, 200);
    Dev(0).AbsMotion(5000, 1000, valuators);

    ASSERT_EVENT(XEvent, motion, dpy, motion_type);
    XDeviceMotionEvent *m = reinterpret_cast<XDeviceMotionEvent*>(motion);
    ASSERT_EQ(m->first_axis, 0);
    ASSERT_EQ(m->axes_count, 5);
    ASSERT_EQ(m->axis_data[0], 5000);
    ASSERT_EQ(m->axis_data[1], 1000);
    ASSERT_EQ(m->axis_data[2], 0);
    ASSERT_EQ(m->axis_data[3], 0);
    ASSERT_EQ(m->axis_data[4], 200);

    XCloseDevice(dpy, xdev);
}

enum scoll_direction {
    VSCROLL_UP = 4,
    VSCROLL_DOWN = 5,
    HSCROLL_LEFT = 6,
    HSCROLL_RIGHT = 7,
};

class SmoothScrollingTest : public PointerMotionTest,
                            public ::testing::WithParamInterface<enum scoll_direction> {
public:
    static constexpr int SCROLL_AXIS_HORIZ = 2;
    static constexpr int SCROLL_AXIS_VERT = 3;

    virtual int GetScrollIncrement(::Display *dpy, int deviceid, int type) {
        int increment = 0;
        int ndevices;

        XIDeviceInfo *info = XIQueryDevice(dpy, deviceid, &ndevices);

        for (int i = 0; i < info->num_classes; i++) {
            if (info->classes[i]->type != XIScrollClass)
                continue;

            XIScrollClassInfo *s = reinterpret_cast<XIScrollClassInfo*>(info->classes[i]);
            if (s->scroll_type == type) {
                increment = s->increment;
                break;
            }
        }

        XIFreeDeviceInfo(info);
        return increment;
    }

    virtual void AssertEmulatedButtonEvent(::Display *dpy, int button) {
        ASSERT_EVENT(XIDeviceEvent, press, dpy, GenericEvent, xi2_opcode, XI_ButtonPress);
        ASSERT_EVENT(XIDeviceEvent, release, dpy, GenericEvent, xi2_opcode, XI_ButtonRelease);
        ASSERT_TRUE(press->flags & XIPointerEmulated);
        ASSERT_TRUE(release->flags & XIPointerEmulated);
        ASSERT_EQ(press->detail, button);
        ASSERT_EQ(release->detail, button);
    }

    virtual void CheckMotionEvent(XIDeviceEvent *motion) {
        ASSERT_FALSE(motion->flags & XIPointerEmulated);
        ASSERT_GE(motion->valuators.mask_len, XIMaskLen(3));
        /* expect x/y to be unset */
        ASSERT_FALSE(XIMaskIsSet(motion->valuators.mask, 0));
        ASSERT_FALSE(XIMaskIsSet(motion->valuators.mask, 1));
    }
};

TEST_P(SmoothScrollingTest, SmoothScrolling)
{
    XORG_TESTCASE("Create a pointer device.\n"
                  "Generate smooth scroll events.\n"
                  "Verify button and motion events are sent as expected\n");

    ::Display *dpy = Display();

    int which = GetParam();

    int deviceid;
    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);

    int button;
    int axis;
    int scroll_amount;

    switch(which) {
        case VSCROLL_UP:
            button = 4;
            axis = SCROLL_AXIS_VERT;
            scroll_amount = -GetScrollIncrement(dpy, deviceid, XIScrollTypeVertical);
            break;
        case VSCROLL_DOWN:
            button = 5;
            axis = SCROLL_AXIS_VERT;
            scroll_amount = GetScrollIncrement(dpy, deviceid, XIScrollTypeVertical);
            break;
        case HSCROLL_LEFT:
            button = 6;
            axis = SCROLL_AXIS_HORIZ;
            scroll_amount = -GetScrollIncrement(dpy, deviceid, XIScrollTypeHorizontal);
            break;
        case HSCROLL_RIGHT:
            button = 7;
            axis = SCROLL_AXIS_HORIZ;
            scroll_amount = GetScrollIncrement(dpy, deviceid, XIScrollTypeHorizontal);
            break;
        default:
            FAIL();
    }

    SelectXI2Events(dpy, VIRTUAL_CORE_POINTER_ID, DefaultRootWindow(dpy),
                    { XI_ButtonPress, XI_ButtonRelease, XI_Motion });

    xorg::testing::inputtest::Valuators valuators;
    valuators.Set(axis, scroll_amount);
    Dev(0).RelMotion(0, 0, valuators);
    Dev(0).RelMotion(0, 0, valuators);

    ASSERT_EVENT(XIDeviceEvent, motion1, dpy, GenericEvent, xi2_opcode, XI_Motion);
    CheckMotionEvent(motion1);
    AssertEmulatedButtonEvent(dpy, button);

    ASSERT_EVENT(XIDeviceEvent, motion2, dpy, GenericEvent, xi2_opcode, XI_Motion);
    CheckMotionEvent(motion2);
    AssertEmulatedButtonEvent(dpy, button);

    /* expect x/y to be unset */
    ASSERT_TRUE(XIMaskIsSet(motion1->valuators.mask, axis));
    ASSERT_TRUE(XIMaskIsSet(motion2->valuators.mask, axis));

    ASSERT_EQ(motion2->valuators.values[0] - motion1->valuators.values[0], scroll_amount);
}

TEST_P(SmoothScrollingTest, SmoothScrollingButtonInverted)
{
    XORG_TESTCASE("Create a pointer device.\n"
                  "Invert hscroll and vscroll mapping\n"
                  "Generate smooth scroll events.\n"
                  "Verify button mapping is reflected in smooth scrolling events\n");

    ::Display *dpy = Display();

    int which = GetParam();

    int deviceid;
    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);

    XDevice *d = XOpenDevice(dpy, deviceid);
    ASSERT_TRUE(d);

    unsigned char map[32];
    int nmap = 32;
    XGetDeviceButtonMapping(dpy, d, map, nmap);
    map[3] = 5;
    map[4] = 4;
    map[5] = 7;
    map[6] = 6;
    XSetDeviceButtonMapping(dpy, d, map, nmap);
    XCloseDevice(dpy, d);

    int button;
    int axis;
    int scroll_amount;

    switch(which) {
        case VSCROLL_UP:
            button = 5;
            axis = SCROLL_AXIS_VERT;
            scroll_amount = -GetScrollIncrement(dpy, deviceid, XIScrollTypeVertical);
            break;
        case VSCROLL_DOWN:
            button = 4;
            axis = SCROLL_AXIS_VERT;
            scroll_amount = GetScrollIncrement(dpy, deviceid, XIScrollTypeVertical);
            break;
        case HSCROLL_LEFT:
            button = 7;
            axis = SCROLL_AXIS_HORIZ;
            scroll_amount = -GetScrollIncrement(dpy, deviceid, XIScrollTypeHorizontal);
            break;
        case HSCROLL_RIGHT:
            button = 6;
            axis = SCROLL_AXIS_HORIZ;
            scroll_amount = GetScrollIncrement(dpy, deviceid, XIScrollTypeHorizontal);
            break;
        default:
            FAIL();
    }

    SelectXI2Events(dpy, VIRTUAL_CORE_POINTER_ID, DefaultRootWindow(dpy),
                    { XI_ButtonPress, XI_ButtonRelease, XI_Motion });

    xorg::testing::inputtest::Valuators valuators;
    valuators.Set(axis, scroll_amount);
    Dev(0).RelMotion(0, 0, valuators);
    Dev(0).RelMotion(0, 0, valuators);

    ASSERT_EVENT(XIDeviceEvent, motion1, dpy, GenericEvent, xi2_opcode, XI_Motion);
    CheckMotionEvent(motion1);
    AssertEmulatedButtonEvent(dpy, button);

    ASSERT_EVENT(XIDeviceEvent, motion2, dpy, GenericEvent, xi2_opcode, XI_Motion);
    CheckMotionEvent(motion2);
    AssertEmulatedButtonEvent(dpy, button);

    /* expect x/y to be unset */
    ASSERT_TRUE(XIMaskIsSet(motion1->valuators.mask, axis));
    ASSERT_TRUE(XIMaskIsSet(motion1->valuators.mask, axis));

    ASSERT_EQ(motion2->valuators.values[0] - motion1->valuators.values[0], scroll_amount);
}

INSTANTIATE_TEST_CASE_P(, SmoothScrollingTest,
                        ::testing::Values(VSCROLL_UP, VSCROLL_DOWN, HSCROLL_LEFT, HSCROLL_RIGHT));

TEST_F(PointerMotionTest, PointerAccelerationIdenticalMovementVerticalHorizontal)
{
    XORG_TESTCASE("Start server with devices in mode relative\n"
                  "Move the pointer diagonally down\n"
                  "Verify x and y relative movement are the same\n"
                  "https://bugs.freedesktop.org/show_bug.cgi?id=31636");

    ::Display *dpy = Display();

    double x1, y1;
    QueryPointerPosition(dpy, &x1, &y1);

    for (int i = 0; i < 20; i++)
        Dev(0).RelMotion(1, 1);

    double x2, y2;
    QueryPointerPosition(dpy, &x2, &y2);

    /* have we moved at all? */
    ASSERT_NE(x1, x2);
    ASSERT_NE(y1, y2);

    /* we moved diagonally, expect same accel in both directions */
    ASSERT_EQ(x2 - x1, y2 - y1);
}

TEST_F(PointerMotionAbsoluteTest, PointerAccelerationIdenticalMovementVerticalHorizontal)
{
    XORG_TESTCASE("Start server with devices in mode absolute\n"
                  "Move the pointer diagonally down\n"
                  "Verify x and y relative movement are the same\n"
                  "https://bugs.freedesktop.org/show_bug.cgi?id=31636");

    ::Display *dpy = Display();

    int x = 1000;
    int y = 1000;

    Dev(0).AbsMotion(x, y);

    double x1, y1;
    QueryPointerPosition(dpy, &x1, &y1);

    while (x < 10000)
    {
        x += 250;
        y += 250;
        Dev(0).AbsMotion(x, y);
    }

    double x2, y2;
    QueryPointerPosition(dpy, &x2, &y2);

    /* have we moved at all? */
    ASSERT_NE(x1, x2);
    ASSERT_NE(y1, y2);

    /* we moved diagonally, expect same accel in both directions.
       Note that the inputtest driver uses the same resolution for both X and Y directions which
       may not match the screen resolution
    */
    auto diff = (x2 - x1) - (y2 - y1) * DisplayWidth(dpy, DefaultScreen(dpy)) /
                                        DisplayHeight(dpy, DefaultScreen(dpy));
    ASSERT_LE(std::abs(diff), 1);
}

class PointerButtonMotionMaskTest : public PointerMotionTest,
                                    public ::testing::WithParamInterface<int>
{
};

TEST_P(PointerButtonMotionMaskTest, ButtonXMotionMask)
{
    XORG_TESTCASE("Select for ButtonXMotionMask\n"
                  "Move pointer\n"
                  "Verify no events received\n"
                  "Press button and move\n"
                  "Verify events received\n");
    int button = GetParam();

    ::Display *dpy = Display();
    XSelectInput(dpy, DefaultRootWindow(dpy), Button1MotionMask << (button - 1));
    XSync(dpy, False);

    Dev(0).RelMotion(10, 0);
    XSync(dpy, False);
    ASSERT_EQ(XPending(dpy), 0);

    Dev(0).ButtonDown(button);
    Dev(0).RelMotion(10, 0);
    Dev(0).ButtonUp(button);

    ASSERT_EVENT(XEvent, motion, dpy, MotionNotify);
    ASSERT_TRUE(motion->xmotion.state & (Button1Mask << (button -1)));

    XSync(dpy, False);
    ASSERT_EQ(XPending(dpy), 0);
}

INSTANTIATE_TEST_CASE_P(, PointerButtonMotionMaskTest, ::testing::Range(1, 4));

class PointerSubpixelTest : public XITServerInputTest, public DeviceInputTestInterface {
public:
    /**
     * Initializes a standard mouse device.
     */
    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER);
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        XITServerInputTest::SetUp();
    }

    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"ConstantDeceleration\" \"20\"\n" +
                               Dev(0).GetOptions());
        // add default keyboard device
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-kbd--",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(1).GetOptions());
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};

TEST_F(PointerSubpixelTest, SubpixelCoreEvents_Incorrect)
{
    XORG_TESTCASE("Move pointer by less than a pixels\n"
                  "Ensure core motion events are received"
                  "(we should not get such events, but this has not yet been implemented)\n"
                  "Ensure XI2 motion events are received\n");

    ::Display *dpy = Display();
    ::Display *dpy2 = XOpenDisplay(server.GetDisplayString().c_str());
    ASSERT_TRUE(dpy2);

    double x, y;
    QueryPointerPosition(dpy, &x, &y);

    XSelectInput(dpy, DefaultRootWindow(dpy), PointerMotionMask);
    XSync(dpy, False);

    Dev(0).RelMotion(0.1, 0);
    Dev(0).RelMotion(0.1, 0);
    Dev(0).RelMotion(0.1, 0);
    Dev(0).RelMotion(0.1, 0);

    ASSERT_TRUE(XServer::WaitForEventOfType(dpy, MotionNotify));

    XSelectInput(dpy, DefaultRootWindow(dpy), NoEventMask);
    XSync(dpy, False);

    XIEventMask mask;
    mask.deviceid = XIAllMasterDevices;
    mask.mask_len = XIMaskLen(XI_Motion);
    mask.mask = new unsigned char[mask.mask_len]();
    XISetMask(mask.mask, XI_Motion);

    XISelectEvents(dpy2, DefaultRootWindow(dpy), &mask, 1);
    XSync(dpy2, False);

    delete[] mask.mask;

    Dev(0).RelMotion(0.1, 0);
    Dev(0).RelMotion(0.1, 0);
    Dev(0).RelMotion(0.1, 0);
    Dev(0).RelMotion(0.1, 0);

    ASSERT_TRUE(XServer::WaitForEventOfType(dpy2, GenericEvent, xi2_opcode, XI_Motion));

    double new_x, new_y;
    QueryPointerPosition(dpy, &new_x, &new_y);
    ASSERT_EQ(x, new_x);
    ASSERT_EQ(y, new_y);

    XCloseDisplay(dpy2);
}

class PointerRelativeTransformationMatrixTest : public PointerMotionTest {
public:
    void SetDeviceMatrix(::Display *dpy, int deviceid, struct pixman_f_transform *m) {
        float matrix[9];

        for (int i = 0; i < 9; i++)
            matrix[i] = m->m[i/3][i%3];

        Atom prop = XInternAtom(dpy, "Coordinate Transformation Matrix", True);
        XIChangeProperty(dpy, deviceid, prop, XInternAtom(dpy, "FLOAT", True),
                         32, PropModeReplace,
                         reinterpret_cast<unsigned char*>(matrix), 9);
    }

    void DisablePtrAccel(::Display *dpy, int deviceid) {
        int data = -1;

        Atom prop = XInternAtom(dpy, "Device Accel Profile", True);
        XIChangeProperty(dpy, deviceid, prop, XA_INTEGER, 32,
                         PropModeReplace, reinterpret_cast<unsigned char*>(&data), 1);
    }

    void MoveAndCompare(::Display *dpy, int dx, int dy) {
        double x, y;

        QueryPointerPosition(dpy, &x, &y);
        Dev(0).RelMotion(dx, dy);
        ASSERT_EVENT(XIDeviceEvent, motion, dpy, GenericEvent, xi2_opcode, XI_Motion);
        ASSERT_EQ(motion->root_x, x + dx);
        ASSERT_EQ(motion->root_y, y + dy);
    }
};


TEST_F(PointerRelativeTransformationMatrixTest, IgnoreTranslationComponent)
{
    XORG_TESTCASE("Apply a translation matrix to the device\n"
                  "Move the pointer.\n"
                  "Verify matrix does not affect movement\n");

    ::Display *dpy = Display();

    int deviceid;
    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);

    XIEventMask mask;
    mask.deviceid = XIAllMasterDevices;
    mask.mask_len = XIMaskLen(XI_Motion);
    mask.mask = new unsigned char[mask.mask_len]();
    XISetMask(mask.mask, XI_Motion);

    XISelectEvents(dpy, DefaultRootWindow(dpy), &mask, 1);

    DisablePtrAccel(dpy, deviceid);

    pixman_f_transform m;
    pixman_f_transform_init_translate(&m, 10, 0);
    SetDeviceMatrix(dpy, deviceid, &m);

    MoveAndCompare(dpy, 10, 0);
    MoveAndCompare(dpy, 0, 10);
    MoveAndCompare(dpy, 5, 5);

    pixman_f_transform_init_translate(&m, 0, 10);
    SetDeviceMatrix(dpy, deviceid, &m);

    MoveAndCompare(dpy, 10, 0);
    MoveAndCompare(dpy, 0, 10);
    MoveAndCompare(dpy, 5, 5);
}

class PointerRelativeRotationMatrixTest : public PointerRelativeTransformationMatrixTest,
                                          public ::testing::WithParamInterface<int> {
};

TEST_P(PointerRelativeRotationMatrixTest, RotationTest)
{
    XORG_TESTCASE("Apply a coordinate transformation to the relative device\n"
                  "Move the pointer.\n"
                  "Verify movement against matrix\n");

    ::Display *dpy = Display();

    int deviceid;
    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);

    XIEventMask mask;
    mask.deviceid = XIAllMasterDevices;
    mask.mask_len = XIMaskLen(XI_Motion);
    mask.mask = new unsigned char[mask.mask_len]();
    XISetMask(mask.mask, XI_Motion);

    XISelectEvents(dpy, DefaultRootWindow(dpy), &mask, 1);

    int angle = GetParam() * M_PI/180.0;

    struct pixman_f_transform m;
    pixman_f_transform_init_rotate(&m, cos(angle), sin(angle));

    SetDeviceMatrix(dpy, deviceid, &m);

    int coords[][2] = {
        {1, 0}, {-2, 0}, {3, 2}, {4, -7},
        {-3, 6}, {-5, -9}, {0, 3}, {0, -5},
        {0, 0}, /* null-terminated */
    };

    int i = 0;
    while(coords[i][0] && coords[i][1]) {
        double dx = coords[i][0], dy = coords[i][1];
        struct pixman_f_vector p;
        p.v[0] = dx;
        p.v[1] = dy;
        p.v[2] = 1;

        ASSERT_TRUE(pixman_f_transform_point(&m, &p));

        double x, y;
        QueryPointerPosition(dpy, &x, &y);

        Dev(0).RelMotion(dx, dy);

        /* Compare to two decimal places */
        ASSERT_EVENT(XIDeviceEvent, motion, dpy, GenericEvent, xi2_opcode, XI_Motion);
        ASSERT_LT(fabs(motion->root_x - (x + p.v[0])), 0.001);
        ASSERT_LT(fabs(motion->root_y - (y + p.v[1])), 0.001);
        i++;
    }

}

INSTANTIATE_TEST_CASE_P(, PointerRelativeRotationMatrixTest, ::testing::Range(0, 360, 15));

class DeviceChangedTest : public XITServerInputTest, public DeviceInputTestInterface {
public:

    xorg::testing::inputtest::Device& TouchDev() { return Dev(0); }
    xorg::testing::inputtest::Device& MouseDev() { return Dev(1); }
    xorg::testing::inputtest::Device& KeyboardDev() { return Dev(2); }

    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::TOUCH);
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER);
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);

        xi2_major_minimum = 2;
        xi2_minor_minimum = 2;
        XITServerInputTest::SetUp();
    }

    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"CorePointer\" \"on\"\n" +
                               Dev(0).GetOptions());
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-mouse--",
                               "Option \"CorePointer\" \"on\"\n" +
                               Dev(1).GetOptions());
        /* add default keyboard device */
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-kbd--",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(2).GetOptions());
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};


TEST_F(DeviceChangedTest, DeviceChangedEvent)
{
    XORG_TESTCASE("Create touchpad and mouse.\n"
                  "Scroll down (smooth scrolling) on touchpad\n"
                  "Move mouse\n"
                  "Move touchpad\n"
                  "DeviceChangedEvent from touchpad must have last smooth scroll valuators set\n"
                  "https://bugs.freedesktop.org/show_bug.cgi?id=62321");

    ::Display *dpy = Display();

    int ndevices;
    XIDeviceInfo *info = XIQueryDevice(dpy, XIAllDevices, &ndevices);
    ASSERT_EQ(ndevices, 7); /* VCP, VCK, xtest * 2, touchpad, mouse, kbd */

    int vscroll_axis = -1;
    int deviceid = -1;

    for (int i = 0; deviceid == -1 && i < ndevices; i++) {
        if (strcmp(info[i].name, "--device--"))
            continue;

        deviceid = info[i].deviceid;
        XIDeviceInfo *di = &info[i];
        for (int j = 0; j < di->num_classes; j++) {
            if (di->classes[j]->type != XIScrollClass)
                continue;

            XIScrollClassInfo *scroll = reinterpret_cast<XIScrollClassInfo*>(di->classes[j]);
            if (scroll->scroll_type == XIScrollTypeVertical) {
                vscroll_axis = scroll->number;
                break;
            }
        }
    }
    XIFreeDeviceInfo(info);

    ASSERT_GT(vscroll_axis, -1);

    XIEventMask mask;
    mask.deviceid = XIAllMasterDevices;
    mask.mask_len = XIMaskLen(XI_Motion);
    mask.mask = new unsigned char[mask.mask_len]();
    XISetMask(mask.mask, XI_ButtonPress);
    XISetMask(mask.mask, XI_Motion);
    XISetMask(mask.mask, XI_DeviceChanged);
    XISelectEvents(dpy, DefaultRootWindow(dpy), &mask, 1);

    xorg::testing::inputtest::Valuators event_valuators;
    event_valuators.Set(2, 0);
    event_valuators.Set(3, 10);
    TouchDev().TouchBegin(100, 100, 0, event_valuators);
    TouchDev().TouchUpdate(120, 120, 0);
    TouchDev().TouchEnd(120, 120, 0);

    ASSERT_EVENT(XIDeviceChangedEvent, dce1, dpy, GenericEvent, xi2_opcode, XI_DeviceChanged);
    ASSERT_EVENT(XIDeviceEvent, motion1, dpy, GenericEvent, xi2_opcode, XI_Motion);
    ASSERT_EVENT(XIDeviceEvent, press1, dpy, GenericEvent, xi2_opcode, XI_ButtonPress);
    ASSERT_EVENT(XIDeviceEvent, motion2, dpy, GenericEvent, xi2_opcode, XI_Motion);
    ASSERT_EVENT(XIDeviceEvent, motion3, dpy, GenericEvent, xi2_opcode, XI_Motion);
    ASSERT_GT(motion1->valuators.mask_len, 0);
    ASSERT_TRUE(XIMaskIsSet(motion1->valuators.mask, vscroll_axis));

    double last_value;
    double *valuators = motion1->valuators.values;

    for (int i = 0; i < vscroll_axis; i++) {
        if (XIMaskIsSet(motion1->valuators.mask, i))
            valuators++;
    }

    last_value = *valuators;
    ASSERT_GT(last_value, 0);

    MouseDev().RelMotion(1, 0);

    ASSERT_EVENT(XIDeviceChangedEvent, dce2, dpy, GenericEvent, xi2_opcode, XI_DeviceChanged);
    ASSERT_EVENT(XIDeviceEvent, motion4, dpy, GenericEvent, xi2_opcode, XI_Motion);

    TouchDev().TouchBegin(100, 100, 0, event_valuators);
    TouchDev().TouchUpdate(120, 120, 0);
    TouchDev().TouchEnd(120, 120, 0);

    ASSERT_EVENT(XIDeviceChangedEvent, dce_final, dpy, GenericEvent, xi2_opcode, XI_DeviceChanged);
    ASSERT_EQ(dce_final->sourceid, deviceid);
    ASSERT_EQ(dce_final->reason, XISlaveSwitch);

    ASSERT_GT(dce_final->num_classes, 1);

    for (int i = 0; i < dce_final->num_classes; i++) {
        if (dce_final->classes[i]->type != XIValuatorClass)
            continue;

        XIValuatorClassInfo *v = reinterpret_cast<XIValuatorClassInfo*>(dce_final->classes[i]);
        if (v->number != vscroll_axis)
            continue;
        ASSERT_EQ(v->value, last_value);
        return;
    }

    FAIL() << "Failed to find vscroll axis in DCE";
}

enum MatrixType {
    IDENTITY,
    LEFT_HALF,
    RIGHT_HALF,
};

class PointerAbsoluteTransformationMatrixTest : public XITServerInputTest,
                                                public DeviceInputTestInterface,
                                                public ::testing::WithParamInterface<std::tuple<enum ::MatrixType, int> > {
public:
    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER_ABSOLUTE);
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        XITServerInputTest::SetUp();
    }

    void SetUpConfigAndLog() override {
        std::tuple<enum MatrixType, int> t = GetParam();
        MatrixType matrix_type = std::get<0>(t);
        int nscreens = std::get<1>(t);

        std::string matrix;
        switch(matrix_type) {
            case LEFT_HALF:
                matrix = "0.5 0 0 0 1 0 0 0 1";
                break;
            case RIGHT_HALF:
                matrix = "0.5 0 0.5 0 1 0 0 0 1";
                break;
            case IDENTITY:
                matrix = "1 0 0 0 1 0 0 0 1";
                break;
        }

        bool reference_from_layout = false;
        switch(nscreens) {
            case 1:
                config.AddDefaultScreenWithDriver();
                reference_from_layout = true;
                break;
            case 2:
                config.SetAutoAddDevices(true);
                config.AppendRawConfig(std::string() +
                    "Section \"ServerLayout\"\n"
                    "	Identifier     \"X.org Configured\"\n"
                    "	Screen         0 \"Screen0\"\n"
                    "	Screen         1 \"Screen1\" RightOf \"Screen0\"\n"
                    "	Option         \"Xinerama\" \"off\"\n"
                    "   Option         \"AutoAddDevices\" \"off\"\n"
                    "EndSection\n"
                    "\n"
                    "Section \"Device\"\n"
                    "	Identifier  \"Card0\"\n"
                    "	Driver      \"dummy\"\n"
                    "EndSection\n"
                    "\n"
                    "Section \"Device\"\n"
                    "	Identifier  \"Card1\"\n"
                    "	Driver      \"dummy\"\n"
                    "EndSection\n"
                    "\n"
                    "Section \"Screen\"\n"
                    "	Identifier \"Screen0\"\n"
                    "	Device     \"Card0\"\n"
                    "EndSection\n"
                    "\n"
                    "Section \"Screen\"\n"
                    "	Identifier \"Screen1\"\n"
                    "	Device     \"Card1\"\n"
                    "EndSection");
                reference_from_layout = false;
                break;
            default:
                    FAIL();
        }
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"CorePointer\" \"on\"\n"
                               "Option \"TransformationMatrix\" \"" + matrix +"\"\n" +
                               Dev(0).GetOptions(),
                               reference_from_layout);
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "kbd-device",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(1).GetOptions(),
                               reference_from_layout);
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};


TEST_P(PointerAbsoluteTransformationMatrixTest, XI2ValuatorData)
{
    std::tuple<enum MatrixType, int> t = GetParam();
    enum MatrixType mtype = std::get<0>(t);
    int nscreens = std::get<1>(t);

    std::string matrix;
    switch(mtype) {
        case IDENTITY: matrix = "identity matrix"; break;
        case LEFT_HALF: matrix = "left half"; break;
        case RIGHT_HALF: matrix = "right half"; break;
    }
    std::stringstream screens;
    screens << nscreens;

    XORG_TESTCASE("Set transformation matrix on absolute tablet\n"
                  "XI2 valuator data must match input data\n"
                  "https://bugs.freedesktop.org/show_bug.cgi?id=63098\n");
    SCOPED_TRACE("Matrix: " + matrix);
    SCOPED_TRACE("nscreens: " + screens.str());


    ::Display *dpy = Display();

    /* Can't register for XIAllDevices, on a dual-head crossing into the new
       stream will generate an XTest Pointer event */
    int deviceid;
    ASSERT_TRUE(FindInputDeviceByName(dpy, "--device--", &deviceid));

    XIEventMask mask;
    mask.deviceid = deviceid;
    mask.mask_len = XIMaskLen(XI_Motion);
    mask.mask = new unsigned char[mask.mask_len]();
    XISetMask(mask.mask, XI_Motion);
    XISetMask(mask.mask, XI_ButtonPress);
    XISetMask(mask.mask, XI_ButtonRelease);

    XISelectEvents(dpy, RootWindow(dpy, 0), &mask, 1);
    if (nscreens > 1)
        XISelectEvents(dpy, RootWindow(dpy, 1), &mask, 1);
    XSync(dpy, False);
    delete[] mask.mask;

    int w = DisplayWidth(dpy, 0);

    // these are hardcoded into inputtest driver. TODO: expose in header
    double minx = 0, maxx = 0xffff;

    Dev(0).AbsMotion(1000, 1000);

    /* drop first event */
    ASSERT_EVENT(XIDeviceEvent, m, dpy, GenericEvent, xi2_opcode, XI_Motion);

    Dev(0).AbsMotion(minx, 1000);

    double expected_minx = minx,
           expected_maxx = maxx;
    switch (mtype) {
        /* no matrix, we expect the device range */
        case IDENTITY:
            if (nscreens == 2)
                expected_maxx = maxx - 1;
            break;
        /* left half we expect the device to go min-max/2 on a single-screen
           setup. dual-screen is per-device range*/
        case LEFT_HALF:
            if (nscreens == 1)
                expected_maxx = maxx / 2;
            break;
        /* right half we expect the device to go from something smaller than
           max/2 to max. the device has 64 units per pixel, if we map to
           512 on the screen that's not maxx/2, it's something less than
           that (see the scaling formula) */
        case RIGHT_HALF:
            if (nscreens == 1)
                expected_minx = w/2.0 * (maxx + 1)/w;
            else {
            /*  problem: scale to desktop == 1024
                re-scale to device == doesn't give us minx because the
                half-point of the display may not be the exact half-point of
                the device if the device res is > screen res.

                device range:   [.............|.............]
                                              ^ device range/2
                pixels: ...[    ][    ][    ]|[     ][     ][     ]...
                                             ^ 0/0 on screen2
                so by scaling from desktop/2 (0/0 on S2) we re-scale into a
                device range that isn't exactly 0 because that device
                coordinate would resolve to pixel desktop/2-1.

                so we expect minx to be _less_ than what would be one-pixel
                on the device.
             */
                expected_minx = maxx/(w-1);
                expected_maxx = maxx - 1;
            }
            break;
    }

    ASSERT_EVENT(XIDeviceEvent, m1, dpy, GenericEvent, xi2_opcode, XI_Motion);
    ASSERT_EQ(m1->deviceid, deviceid);
    ASSERT_TRUE(XIMaskIsSet(m1->valuators.mask, 0));
    if (mtype == RIGHT_HALF && nscreens > 1)
        ASSERT_TRUE(double_cmp(m1->valuators.values[0], expected_minx) <= 0);
    else
        ASSERT_TRUE(double_cmp(m1->valuators.values[0], expected_minx) == 0);

    /* y will be scaled in some weird way (depending on proportion of
       screen:tablet), so we ignore it here */

    Dev(0).AbsMotion(maxx, 1000);

    ASSERT_EVENT(XIDeviceEvent, m2, dpy, GenericEvent, xi2_opcode, XI_Motion);
    ASSERT_EQ(m1->deviceid, deviceid);
    ASSERT_TRUE(XIMaskIsSet(m2->valuators.mask, 0));
    ASSERT_TRUE(double_cmp(m2->valuators.values[0], expected_maxx) == 0);
}

INSTANTIATE_TEST_CASE_P(, PointerAbsoluteTransformationMatrixTest,
                        ::testing::Combine(::testing::Values(IDENTITY, LEFT_HALF, RIGHT_HALF),
                                           ::testing::Values(1, 2)));


class KeyboardTest : public XITServerInputTest,
                     public DeviceInputTestInterface {
public:
    /**
     * Initializes a standard mouse device.
     */
    void SetUp() override {
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        XITServerInputTest::SetUp();
    }

    /**
     * Sets up an xorg.conf for a single evdev CoreKeyboard device based on
     * the evemu device. The input from GetParam() is used as XkbLayout.
     */
    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device-kbd--",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(0).GetOptions());
        config.WriteConfig();
    }

    void StartServer() override {
        XITServerInputTest::StartServer();
        WaitForDevices();
    }
};

TEST_F(KeyboardTest, FocusTestAttachedSlave)
{
    XORG_TESTCASE("Create window\n"
                  "Set VCK focus to window\n"
                  "Select for VCK keyboard events on window\n"
                  "Press key on slave device\n"
                  "Assert events received on window\n");

    ::Display *dpy = Display();

    int deviceid;
    ASSERT_TRUE(FindInputDeviceByName(dpy, "--device-kbd--", &deviceid));

    Window win = CreateWindow(dpy, None, 0, 0, 10, 10);
    XISetFocus(dpy, VIRTUAL_CORE_KEYBOARD_ID, win, CurrentTime);
    SelectXI2Events(dpy, VIRTUAL_CORE_KEYBOARD_ID, win, { XI_KeyPress, XI_KeyRelease });

    Dev(0).KeyDown(KEY_A);
    Dev(0).KeyUp(KEY_A);

    ASSERT_EVENT(XIDeviceEvent, press, dpy, GenericEvent, xi2_opcode, XI_KeyPress);
    ASSERT_EQ(press->event, win);
    ASSERT_EQ(press->deviceid, VIRTUAL_CORE_KEYBOARD_ID);
    ASSERT_EQ(press->sourceid, deviceid);
    ASSERT_EVENT(XIDeviceEvent, release, dpy, GenericEvent, xi2_opcode, XI_KeyRelease);
    ASSERT_EQ(release->event, win);
    ASSERT_EQ(release->deviceid, VIRTUAL_CORE_KEYBOARD_ID);
    ASSERT_EQ(release->sourceid, deviceid);
}


TEST_F(KeyboardTest, FocusTestAttachedSlaveSeparateFocus)
{
    XORG_TESTCASE("Create window\n"
                  "Set slave focus to window\n"
                  "Select for slave events on window\n"
                  "Press key on slave device\n"
                  "Assert events received on window\n");

    ::Display *dpy = Display();

    int deviceid;
    ASSERT_TRUE(FindInputDeviceByName(dpy, "--device-kbd--", &deviceid));

    Window win = CreateWindow(dpy, None, 0, 0, 10, 10);
    SelectXI2Events(dpy, deviceid, win, { XI_KeyPress, XI_KeyRelease });

    Dev(0).KeyDown(KEY_A);
    Dev(0).KeyUp(KEY_A);

    ASSERT_TRUE(NoEventPending(dpy));

    XISetFocus(dpy, deviceid, win, CurrentTime);
    Dev(0).KeyDown(KEY_A);
    Dev(0).KeyUp(KEY_A);

    ASSERT_EVENT(XIDeviceEvent, press, dpy, GenericEvent, xi2_opcode, XI_KeyPress);
    ASSERT_EQ(press->event, win);
    ASSERT_EQ(press->deviceid, deviceid);
    ASSERT_EVENT(XIDeviceEvent, release, dpy, GenericEvent, xi2_opcode, XI_KeyRelease);
    ASSERT_EQ(release->event, win);
    ASSERT_EQ(release->deviceid, deviceid);
}

TEST_F(KeyboardTest, FocusTestFloatingSlave)
{
    XORG_TESTCASE("Float slave device\n"
                  "Create window\n"
                  "Set VCK focus to window\n"
                  "Select for slave events on window\n"
                  "Press key on slave device\n"
                  "Assert no event received\n"
                  "Set VCK focus to root, slave focus to window\n"
                  "Press key on slave device\n"
                  "Assert event received\n");

    ::Display *dpy = Display();

    int deviceid;
    ASSERT_TRUE(FindInputDeviceByName(dpy, "--device-kbd--", &deviceid));

    XIAnyHierarchyChangeInfo float_slave;
    float_slave.detach.type = XIDetachSlave;
    float_slave.detach.deviceid = deviceid;
    XIChangeHierarchy(dpy, &float_slave, 1);

    Window win = CreateWindow(dpy, None, 0, 0, 10, 10);
    XISetFocus(dpy, VIRTUAL_CORE_KEYBOARD_ID, win, CurrentTime);
    SelectXI2Events(dpy, deviceid, win, { XI_KeyPress, XI_KeyRelease });

    Dev(0).KeyDown(KEY_A);
    Dev(0).KeyUp(KEY_A);

    ASSERT_TRUE(NoEventPending(dpy));

    XISetFocus(dpy, VIRTUAL_CORE_KEYBOARD_ID, DefaultRootWindow(dpy), CurrentTime);
    XISetFocus(dpy, deviceid, win, CurrentTime);

    Dev(0).KeyDown(KEY_A);
    Dev(0).KeyUp(KEY_A);

    ASSERT_EVENT(XIDeviceEvent, press, dpy, GenericEvent, xi2_opcode, XI_KeyPress);
    ASSERT_EQ(press->event, win);
    ASSERT_EQ(press->deviceid, deviceid);
    ASSERT_EVENT(XIDeviceEvent, release, dpy, GenericEvent, xi2_opcode, XI_KeyRelease);
    ASSERT_EQ(release->event, win);
    ASSERT_EQ(release->deviceid, deviceid);
}

class XIQueryVersionTest : public XITServerTest,
                           public ::testing::WithParamInterface<int>
{
    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.WriteConfig();
    }
};

TEST_F(XIQueryVersionTest, OverrideXI2Version)
{
    XORG_TESTCASE("Submit XIQueryVersion request\n"
                  "Resubmit XIQueryVersion request with different XI2 minor\n"
                  "Expect BadValue for mismatch, success otherwise\n");

    int major = 2;
    int max_minor = 25;
    ::Display *test_dpy = XOpenDisplay(server.GetDisplayString().c_str());
    XIQueryVersion(test_dpy, &major, &max_minor);
    XCloseDisplay(test_dpy);

    for (int i = 0; i < XI_2_Minor + 1; i++) {
        ::Display *dpy = XOpenDisplay(server.GetDisplayString().c_str());
        int prev_minor = i;
        XIQueryVersion(dpy, &major, &prev_minor);

        EXPECT_EQ(major, 2);
        if (prev_minor < i)
            break;

        for (int j = 0; j < XI_2_Minor + 1; j++) {
            const XErrorEvent *err;
            int minor = j;
            SetErrorTrap(dpy);
            XIQueryVersion(dpy, &major, &minor);
            EXPECT_EQ(major, 2);
            err = ReleaseErrorTrap(dpy);

            std::stringstream ss; ss << "Requested 2." << prev_minor << ", then 2." << j;
            SCOPED_TRACE(ss.str());

            if (prev_minor < 2 || j < 2) {
                if (j < prev_minor) {
                    ASSERT_ERROR(err, BadValue);
                } else
                    ASSERT_EQ(minor, prev_minor);
            } else { /* both versions are 2.2 or higher */
                ASSERT_EQ(minor, std::min(j, max_minor));
            }
        }

        XCloseDisplay(dpy);
    }
}

TEST_F(XIQueryVersionTest, NoBadLengthOnXIAllowEvents)
{
    XORG_TESTCASE("Submit XIQueryVersion request\n"
                  "Call XIAllowEvents\n"
                  "Ensure no BadLength is received\n"
                  "https://bugs.freedesktop.org/show_bug.cgi?id=68554");


    int major = 2;
    int minor = 2;
    ::Display *test_dpy = XOpenDisplay(server.GetDisplayString().c_str());
    XIQueryVersion(test_dpy, &major, &minor);
    XCloseDisplay(test_dpy);
    ASSERT_GE(minor, 2); /* server must provide 2.2 at least */

    ::Display *dpy = Display();
    minor = 0;
    XIQueryVersion(dpy, &major, &minor);

    SetErrorTrap(dpy);
    XIAllowEvents(dpy, 2, XIAsyncDevice, CurrentTime);
    ASSERT_NO_ERROR(ReleaseErrorTrap(dpy));
    XSync(dpy, False);
}

class FloatingSlaveTest : public XITServerInputTest,
                          public DeviceInputTestInterface,
                          public ::testing::WithParamInterface<xorg::testing::inputtest::DeviceType>
{
    void SetUp() override {
        AddDevice(GetParam());
        AddDevice(xorg::testing::inputtest::DeviceType::KEYBOARD);
        AddDevice(xorg::testing::inputtest::DeviceType::POINTER);
        XITServerInputTest::SetUp();
    }

    void SetUpConfigAndLog() override {
        config.AddDefaultScreenWithDriver();
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "--device--",
                               "Option \"Floating\" \"on\"\n" +
                               Dev(0).GetOptions());
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "kbd-device",
                               "Option \"CoreKeyboard\" \"on\"\n" +
                               Dev(1).GetOptions());
        config.AddInputSection(XORG_INPUTTEST_DRIVER, "mouse-device",
                               "Option \"CorePointer\" \"on\"\n" +
                               Dev(2).GetOptions());
        config.WriteConfig();
    }
};

TEST_P(FloatingSlaveTest, NoCrashOnStartup)
{
    XORG_TESTCASE("Start with a floating pointer device\n"
                  "Expect no crash\n");

    ::Display *dpy = Display();
    int deviceid;
    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);
}

TEST_P(FloatingSlaveTest, DisableEnableDevice)
{
    XORG_TESTCASE("Start with a floating pointer device\n"
                  "Disable, re-enable device\n"
                  "Expect no crash\n");

    ::Display *dpy = Display();
    int deviceid;

    ASSERT_EQ(FindInputDeviceByName(dpy, "--device--", &deviceid), 1);

    ASSERT_PROPERTY(unsigned char, enabled_prop, dpy, deviceid, "Device Enabled");
    enabled_prop.data[0] = 0;
    enabled_prop.Update();
    XSync(dpy, False);
    enabled_prop.data[0] = 1;
    enabled_prop.Update();
    XSync(dpy, False);
}

INSTANTIATE_TEST_CASE_P(, FloatingSlaveTest, ::testing::Values(
                            xorg::testing::inputtest::DeviceType::POINTER,
                            xorg::testing::inputtest::DeviceType::KEYBOARD));
