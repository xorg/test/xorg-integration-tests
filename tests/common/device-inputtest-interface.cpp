/*
 * Copyright (C) 2020 Povilas Kanapickas <povilas@radix.lt>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

#include "device-inputtest-interface.h"
#include <stdexcept>
#include <string>

namespace {
    // FIXME: this should be replaced with std::filesystem::temp_directory_path() when we can use
    // c++17
    std::string GetTemporaryDirectoryPath()
    {
        const char* env_variables[] = {
            "TMPDIR",
            "TMP",
            "TEMP",
            "TEMPDIR"
        };
        for (const char* env : env_variables) {
            const char* path = std::getenv(env);
            if (path)
                return path;
        }
        return "/tmp";
    }

    std::string GetSocketPath(const std::string& identifier)
    {
        return GetTemporaryDirectoryPath() + "/" + identifier;
    }

    int s_device_counter = 0;
} // namespace

void DeviceInputTestInterface::AddDevice(xorg::testing::inputtest::DeviceType device_type)
{
    s_device_counter++;
    auto path = GetSocketPath("xit_events_socket_" + std::to_string(s_device_counter) + "_" +
                                  std::to_string(devices.size()));
    devices.push_back(std::unique_ptr<xorg::testing::inputtest::Device>(
        new xorg::testing::inputtest::Device(path, device_type)));
}

xorg::testing::inputtest::Device& DeviceInputTestInterface::Dev(unsigned i)
{
    if (i >= devices.size()) {
        throw std::runtime_error("Device does not exist");
    }
    if (!devices[i]) {
        throw std::runtime_error("Device has been removed");
    }
    return *devices[i];
}

void DeviceInputTestInterface::WaitForDevices()
{
    for (const auto& dev : devices) {
        if (!dev)
            continue;
        dev->WaitForDriver();
    }
}
