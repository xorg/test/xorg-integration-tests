#!/bin/bash

if [[ -z $IN_CONTAINER ]]; then
    echo "Please use start-container.sh script"
    exit
fi

function cleanup {
    rm -rf _xorg_integration_test_empty
}

trap cleanup EXIT

mkdir -p _xorg_integration_test_empty

mount --bind _xorg_integration_test_empty /dev
mount --bind _xorg_integration_test_empty /sys

# Hide any custom configuration on the test box
mount --bind _xorg_integration_test_empty /usr/share/X11/xorg.conf.d

/bin/bash
